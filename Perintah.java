import java.awt.Dimension;          /*** Import Library */
import java.lang.reflect.Array;

/**
 * 
 * Perintah.java
 * <br><br>
 * Class {@code Perintah} merepresentasikan perintah-perintah umum yang 
 * dapat diberikan kepada kura-kura. Termasuk dalam class ini adalah
 * proses untuk membaca input (saat ini baru melalui satu baris perintah)
 * dan memanggil method yang berkesesuaian.
 * Dalam kelas ini juga disediakan method-method yang merupakan kumpulan-kumpulan
 * perintah berurutan yang dapat diterima oleh kurakura dan menghasilkan gambar 
 * tertentu. 
 * <br><br>
 * Tugas anda pada file ini: <br>
 * - Lengkapi javadoc comment pada tiap deklarasi method.<br>
 * - Lengkapi inline comment untuk tiap baris perintah yang penting.<br>
 * - Perbaiki method {@code lakukan} agar tidak menimbulkan error bila input salah<br>
 * - Buat (1) perintah {@code mundur <x>}" agar kura-kura bisa mundur sebanyak x pixel satuan.
 * - Buat (2) perintah {@code hadap kanan} dan {@code hadap kiri} yang akan membuat kura-kura 
 *   menghadap ke kanan (rotasi 90) dan ke kiri (rotasi -90) 
 * - Dapatkah anda membuat (3) kura-kura mengenali perintah 
 *   {@code loop 10 perintah-perintah} <br>
 *   yang artinya melakukan perintah-perintah sebanyak 10 kali? <br>
 *   contoh: "loop 10 rotasi 30 maju 30" <br>
 *           yang artinya akan melakukan perintah "rotasi 30", dilanjutkan <br>
 *           perintah "maju 30", secara berulang-ulang sebanyak 10 kali<br>
 *   contoh: "loop 5 maju 20 hadap kanan maju 30" <br>
 *           yang artinya akan melakukan perintah "maju 20", dilanjutkan <br>
 *           "hadap kanan", kemudian perintah "maju 10", <br> 
 *           secara berulang-ulang sebanyak 5 kali<br>
 * 
 * @author Ade Azurat for DPBO 2008 @ Fasilkom UI
 * @author Ade Azurat for DDP2 2023 @ Fasilkom UI
 */
public class Perintah {
    Canvas canvas;
    Kurakura kurakuraku; 
    
    /*** Creates a new instance of Perintah */
    public Perintah(Kurakura k, Canvas canvas) {
        kurakuraku = k;
        this.canvas = canvas;
    }

    // Dapatkan anda membuat method ini lebih baik dan lebih mudah ditambahkan
    // atau di ubah? 
    public String lakukan(String inputPerintah){
        String[] in = inputPerintah.split(" ");
        try{
            if (in[0].equalsIgnoreCase("selesai"))              /**Index 0 bila sesuai dengan variabel string maka akan memanggil method */
                System.exit(0);                          /** Menutup mainframe */
            else if (in[0].equalsIgnoreCase("reset"))           /**Index 0 bila sesuai dengan variabel string maka akan memanggil method */
                kurakuraku.reset();                             /** Memanggil method reset */
            else if (in[0].equalsIgnoreCase("maju"))            /**Index 0 bila sesuai dengan variabel string maka akan memanggil method */
                kurakuraku.maju(Integer.parseInt(in[1]));       /** Index 1 refer ke parameter panjang maju */
            else if (in[0].equalsIgnoreCase("mundur"))          /**Index 0 bila sesuai dengan variabel string maka akan memanggil method */
                    kurakuraku.mundur(Integer.parseInt(in[1])); /** Index 1 refer ke parameter pajang mundur */
            else if (in[0].equalsIgnoreCase("rotasi"))          /**Index 0 bila sesuai dengan variabel string maka akan memanggil method */
                    kurakuraku.rotasi(Integer.parseInt(in[1])); /** Index 1 refer ke besar sudut rotasi */
            else if (in[0].equalsIgnoreCase("kotak"))           /**Index 0 bila sesuai dengan variabel string maka akan memanggil method */
                    buatKotak(Integer.parseInt(in[1]));         /** Index 1 refer ke panjang sisi kota*/
            else if (in[0].equalsIgnoreCase("persegi"))         /**Index 0 bila sesuai dengan variabel string maka akan memanggil method */
                    buatPersegi(Integer.parseInt(in[1]), Integer.parseInt(in[2]));      /** Index 1 refer ke panjang persegi dan index 2 refer ke lebar persegi */
            else if (in[0].equalsIgnoreCase("segitigasiku"))    /**Index 0 bila sesuai dengan variabel string maka akan memanggil method */
                    buatSegitigaSikuSiku(Integer.parseInt(in[1]), Integer.parseInt(in[2])); /** Index 1 refer ke panjang alas segitiga dan index 2 refer ke tinggi segitiga */
            else if (in[0].equalsIgnoreCase("segitiga"))        /**Index 0 bila sesuai dengan variabel string maka akan memanggil method */
                    buatSegitiga(Integer.parseInt(in[1]));      /** Index 1 refer ke panjang sisi segitiga */
            else if (in[0].equalsIgnoreCase("pohon"))           /**Index 0 bila sesuai dengan variabel string maka akan memanggil method */
                    buatPohon();            /** Memanggil method buatPohon */
            else if (in[0].equalsIgnoreCase("jejak"))           /**Index 0 bila sesuai dengan variabel string maka akan memanggil method */
                    kurakuraku.setJejak(Boolean.parseBoolean(in[1]));
            else if (in[0].equalsIgnoreCase("pindah"))          /**Index 0 bila sesuai dengan variabel string maka akan memanggil method */
                    kurakuraku.setPosition(new Dimension(Integer.parseInt(in[1]),Integer.parseInt(in[2])));
            else if (in[0].equalsIgnoreCase("nestedsq"))          /**Index 0 bila sesuai dengan variabel string maka akan memanggil method */
                    buatNestedSquares(Integer.parseInt(in[1]));
            else if (in[0].equalsIgnoreCase("snow"))          /**Index 0 bila sesuai dengan variabel string maka akan memanggil method */
                    buatSnow(Integer.parseInt(in[1]));
            else if (in[0].equalsIgnoreCase("sierpinski"))          /**Index 0 bila sesuai dengan variabel string maka akan memanggil method */
                    buatSierpinski(Integer.parseInt(in[1]));
            else if (in[0].equalsIgnoreCase("berlian"))          /**Index 0 bila sesuai dengan variabel string maka akan memanggil method */
                    buatBerlian(Integer.parseInt(in[1]));
            else if (in[0].equalsIgnoreCase("terbalik"))          /**Index 0 bila sesuai dengan variabel string maka akan memanggil method */
                    buatSegitigaTerbalik(Integer.parseInt(in[1]));          
            else{
                    canvas.repaint(); 
                    return "Perintah tidak dipahami.";          /** Bila index serta input tidak sesuai maka mereturn teks ini */
                }
                    }
        catch(Exception e){
            canvas.repaint(); 
                    return "Perintah tidak dipahami.";      /** Bila terdapat error maka mereturn teks in */
        }

        canvas.repaint();    
        return "Perintah sudah dilaksanakan.";          /**Bila perintah pada method sudah selesai, maka mereturn teks ini */
    }


    /** Membuat segitiga terbalik */
    public void buatSegitigaTerbalik(int ukuran){
            for (int i =0; i<3; i++){       /** Menggambar sisi segitiga sebanyak 3 lagi */
                kurakuraku.maju(ukuran);    /** Menggambar sisa sisinya */
                kurakuraku.rotasi(-120);    /** Membuat sudut segitiga 180-120=60 */
            }
    }  

    /**Membuat method sierpinski */
    public void buatSierpinski(int ukuran){
        buatSegitigaTerbalik(ukuran);           /** Membuat segitiga utama */
        kurakuraku.maju(ukuran/2);              /** Membuat titik turtle pada tengah sisi segitiga utama */
        sierpinskiRekursif(ukuran/2);           /** Memanggil method sierpinski meenggunakan rekursif */
    }

    /**Membuat method rekursif untuk sierpinski */
    public void sierpinskiRekursif(int ukuran) {
        if (ukuran <= 10) {                     /** Base case */
            return;
        }
        
        else {
        kurakuraku.rotasi(-60);                     /** Rotasi sebesar 60 untuk membuat arah kura-kura sejajar horizontal */
        buatSegitigaTerbalik(ukuran);               /** Membuat segitiga */
        kurakuraku.rotasi(60);                    /** Merotasikan kembali agar posisi kura-kura sejajar */
        Dimension now = kurakuraku.getPosition();   /** Mengeset titik posisi tengah  */
        
        kurakuraku.maju(ukuran/2);                  /** Membuat kura-kura berada pada titik tengah  */
        sierpinskiRekursif(ukuran/2);               /** Rekursi untuk bagian kanan */
        
        kurakuraku.reset();                         /** Mengembalikan posisi kura-kura */
        kurakuraku.setPosition(now);                /** Membuat kura-kura ke posisi now */
        kurakuraku.mundur(ukuran/2);                /** Membuat kura-kura berada di titik tengah lainnya */
        sierpinskiRekursif(ukuran/2);               /** Rekursi untuk bagian kiri */
        
        kurakuraku.reset();                         /** Mengembalikan posisi kura-kura */
        kurakuraku.setPosition(now);                /** Membuat kura-kura ke posisi now */
        kurakuraku.rotasi(-60);                     /** Rotasi sebesar 60 untuk membuat arah kura-kura sejajar horizontal */
        kurakuraku.maju(ukuran);                
        kurakuraku.rotasi(-120);
        kurakuraku.maju(ukuran/2);
        kurakuraku.rotasi(180);
        
        sierpinskiRekursif(ukuran/2);               /** Rekursi untuk bagian atas */
        }
    }
    
    /** Membuat method Berlian */
    public void buatBerlian(int tinggi){    
        int garis = 5;                                     /** Inisiasi variabel ukuran = 5 */
    if (garis > 1){                                        /** Base case */
            kurakuraku.setJejak(true);                   /** Memunculkan jejak  */
            Dimension posAwal = kurakuraku.getPosition();  /** Menentukan posAwal dititik ini */
            double arah = kurakuraku.getArah();            /** Menginisiasi arah  */
            double sudut = arah;                           /** Menginisiasi sudut dari variabel arah */

            for(int i=0;i<12;i++){                         /** Looping untuk banyaknya cabang agar membentuk bangun melingkar */
                buatPohon(garis - 1, (int)(tinggi/1.5));   /** Rekursi untuk banyaknya garis cabang */
                kurakuraku.setJejak(false);              /** Mematikan jejak */
                kurakuraku.setPosition(posAwal);           /** Kembali ke posisi awal */
                kurakuraku.setArah(arah);                  /** Mengeset arah */
                sudut+=30;                                 /** Sudut akan bertambah terus sebanyak 30 derajat */
                kurakuraku.rotasi(sudut);                  /** Berotasi sesuai dengan derajat sudut */
            }     
        }

    else{                                                  /** Diluar base case tidak membuat apa-apa */
        return;
    }
    }

    /**Membuat method snow */
    public void buatSnow(int ukuran){
        if (ukuran >=0){                                    /** Base case */
            Dimension pos = kurakuraku.getPosition();       /** Menentukan titik posisi */
            double arah = kurakuraku.getArah();             /** Menginisiasi variabel arah */

            for (int i=0 ; i<6 ; i++){                      /** Looping untuk banyak cabang */
                kurakuraku.setArah(arah);                   /** Mengeset arah */
                arah = arah+10;                             /** Variabel arah bertambah konstan seiring loopingan */
                kurakuraku.maju(ukuran);                    /** Menggambar garis kedepan */
                buatSnow(ukuran/2 - 2);                     /** Memanggil rekursi snow */
                kurakuraku.setPosition(pos);                /** Mengeset posisi kembali */
                kurakuraku.rotasi(arah);                    /** Berotasi sebesar arah */
            }
        }
    }

    /**Membuat method nested squares */
    public void buatNestedSquares(int ukuran) {
        if (ukuran < 0) {                       /** Base case */
            return;
        }
        
        else {
        buatKotak(ukuran);                      /** Kotak pertama */
        kurakuraku.setJejak(false);           /** Jejak dimatikan */
        kurakuraku.maju(10);              /** Maju sepanjang 10 satuan */
        kurakuraku.rotasi(90);                /** Rotasi 90 derajat agar belok ke dalam */
        kurakuraku.maju(10);              /** Maju kedalam kotak sebelumnya 10 satuan */
        kurakuraku.rotasi(-90);                 /** Rotasi 90 derajat agar kembali ke posisi sejajar dengan kotak sebelumnya*/
        kurakuraku.setJejak(true);            /** Jejak dihidupkan kembali */
        
        buatNestedSquares(ukuran - 20);         /** Rekursif untuk boxes */
        }
    }
    

    /** Method membuat persegi */
    public void buatPersegi(int panjang, int lebar) {       /**set parameter */
        for (int i=0; i<4; i++){                            /** Loopingan untuk banyak sisi */
            if(i%2 == 0){                                   /** Saat i genap maka dia refer ke panjang */
                kurakuraku.maju(panjang);                   /** Maju sepanjang var panjang */
                kurakuraku.rotasi(90);                    /** Rotasi sebesar 90 derajat */
            }
            else{
                kurakuraku.maju(lebar);                     /** Maka refer ke lebar */
                kurakuraku.rotasi(90);                    /** Akan berotasi lagi 90 derajat */
            }
        }
    }

    /** Membuat segitiga siku */
    public void buatSegitigaSikuSiku(double alas, double tinggi){
        kurakuraku.maju(alas);          /** Membuat alas */
        kurakuraku.rotasi(-90);         /** Membuat sudut siku */
        kurakuraku.maju(tinggi);        /** Membuat tinggi */
        double r1, r2, d2;              /** Inisiasi variabel */

        r1=Math.atan((tinggi/alas));    /** Mencari radian sudut r1 */
        r2=((Math.PI)/2)-r1;            /** Radian sudut r2 */
        d2=Math.toDegrees(r2);          /** Mengubah radian menjadi derajat */
        kurakuraku.rotasi(-(180-d2));   /** Merotasi kura-kura untuk menggambar dari titik tinggi terakhir menuju titik awal */
        kurakuraku.maju(alas/Math.cos(r1)); /** Panjang sisi miring */
    }

    /** Method membuat kotak */
    public void buatKotak(int ukuran ){        
        for (int i=0;i<4;i++){          /** Membuat banyak sisi sebanyak 4 */
            kurakuraku.maju(ukuran);    /** Menggambar garis sepanjang ukuran */
            kurakuraku.rotasi(90);    /** Untuk membentuk sudut siku */
        }
    }

    /** Membuat segitiga */
    public void buatSegitiga(int ukuran){
        // TODO: Lengkapi isi method ini agar kura-kura bisa membuat segitiga sama sisi
            kurakuraku.maju(ukuran);        /** Menggambar alas sepanjang ukuran  */
            for (int i =1; i<3; i++){       /** Menggambar sisi segitiga sebanyak 2 lagi */
                kurakuraku.rotasi(-120);    /** Membuat sudut segitiga */
                kurakuraku.maju(ukuran);    /** Menggambar sisa sisinya */
            }
    }    

    /** Membuat pohon */
    public void buatPohon(){        
        kurakuraku.setJejak(false);
        kurakuraku.reset();
        kurakuraku.rotasi(90);      /** Membuat batang vertikal utama */
        kurakuraku.maju(100);   /** sepanjang 100 */
        kurakuraku.rotasi(180);
        buatPohon(6,50);        /** Memanggil fungsi pohon (membuat ranting) */
        kurakuraku.reset();
    }
    
    /** Membuat ranting atau cabang pohon */
    private void buatPohon(int ukuran, int tinggi){
    if (ukuran==1){                                 /** Disaat ukuran 1 */
            kurakuraku.setJejak(true);
            kurakuraku.maju(tinggi);                        /** akan menggambar seperti biasa */
            kurakuraku.rotasi(-45);                         /** untuk membuat ranting dengan jarak sudut 45 derajat */
            Dimension posAwal = kurakuraku.getPosition();
            double arah = kurakuraku.getArah();
            double sudut = arah;
                buatKotak(3);                           /** diujung ranting akan ada kotak berukuran 3 */
   
              
        }   
    else if (ukuran>1){                                         /** saat ukuran >1 */
            kurakuraku.setJejak(true);
            kurakuraku.maju(tinggi);                        /** Akan menggambar ranting */
            kurakuraku.rotasi(-45);                         /** dengan sudut 45 derajat */
            Dimension posAwal = kurakuraku.getPosition();
            double arah = kurakuraku.getArah();
            double sudut = arah;
            for(int i=0;i<3;i++){                           /** Loopingan agar ranting sebatas 3 saja */
                buatPohon(ukuran-1,(int)(tinggi/1.5));      /** Agar cabang ranting dari batang utama bernilai 6 dan ukuran cabang semakin pendek */
                kurakuraku.setJejak(false);
                kurakuraku.setPosition(posAwal);
                kurakuraku.setArah(arah);                
                sudut+=45;                                  /** untuk membentuk sudut */
                kurakuraku.rotasi(sudut);  
               
            }     
        }
    
       
        
        kurakuraku.reset();
    }
}
